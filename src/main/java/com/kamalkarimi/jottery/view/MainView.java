package com.kamalkarimi.jottery.view;


import com.kamalkarimi.jottery.model.CustomerInfo;
import com.kamalkarimi.jottery.model.Level;
import com.kamalkarimi.jottery.model.Lottery;
import com.kamalkarimi.jottery.model.Prize;
import com.kamalkarimi.jottery.services.*;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.router.Route;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@EqualsAndHashCode(callSuper = true)
@Component
@Route("")
@Slf4j
@Data
public class MainView extends VerticalLayout {

    private ServiceManager serviceManager;

    private Long numberOfCustomers;

    private Label labelNumberTotalCustomer;
    private Hr header;

    private Button btnAddLevel;
    private TextField txtLevelName;
    private TextField minLevel;
    private TextField maxLevel;

    private Div divNewLevel = new Div();
    private Div divNewPrize = new Div();

    private Button btnAddPrize;
    private TextField txtCountPrize;
    private TextField txtNamePrize;
    private ComboBox<String> comboBoxLevelPrize = new ComboBox<>("Level");

    private Grid<Level> levelItemGrid = new Grid<>();
    private Grid<CustomerInfo> gridLotteryResult = new Grid<>();
    private Grid<Prize> prizeGrid = new Grid<>();

    private TextField txtStep;
    private TextField txtLotteryName;
    private List<CustomerInfo> winners = new ArrayList<>();
    private Div divLotteryResult = new Div();


    private Button btnProcess;


    @Autowired
    public MainView(ServiceManager serviceManager){
        this.serviceManager = serviceManager;
        setPadding(true);
        setSpacing(true);
        numberOfCustomers = serviceManager.totalCustomer();
        labelNumberTotalCustomer = new Label("Total number of participants: " + numberOfCustomers);

        txtLevelName = new TextField("Level name");
        levelItemGrid.addColumn(Level::getName).setHeader("Level Name").setSortable(true);
        levelItemGrid.addColumn(Level::getMinBound).setHeader("Min").setSortable(true);
        levelItemGrid.addColumn(Level::getMaxBound).setHeader("Max").setSortable(true);

        prizeGrid.addColumn(Prize::getName).setHeader("Prize Name").setSortable(true);
        prizeGrid.addColumn(Prize::getLevel).setHeader("Prize Level").setSortable(true);
        prizeGrid.addColumn(Prize::getCountPrize).setHeader("Prize Count").setSortable(true);

        gridLotteryResult.addColumn(CustomerInfo::getName).setHeader("Name").setSortable(true);
        gridLotteryResult.addColumn(CustomerInfo::getLastName).setHeader("Last Name").setSortable(true);
        gridLotteryResult.addColumn(CustomerInfo::getLevel).setHeader("Level").setSortable(true);
        gridLotteryResult.addColumn(CustomerInfo::getPoint).setHeader("Point").setSortable(true);
        gridLotteryResult.addColumn(CustomerInfo::getPrize).setHeader("Prize").setSortable(true);

        minLevel = new TextField("Min");
        maxLevel = new TextField("Max");
        txtLevelName.setRequired(true);
        minLevel.setRequired(true);
        maxLevel.setRequired(true);
        btnAddLevel = new Button("Add Level", event -> {
            if (txtLevelName.getValue().equalsIgnoreCase("") ||
                    minLevel.getValue().equalsIgnoreCase("") ||
                    maxLevel.getValue().equalsIgnoreCase("")) {
                txtLevelName.setErrorMessage("Required");
                minLevel.setErrorMessage("Required");
                maxLevel.setErrorMessage("Required");
            } else {
                Level levelItem = new Level();
                levelItem.setName(txtLevelName.getValue());
                levelItem.setMaxBound(Integer.parseInt(maxLevel.getValue()));
                levelItem.setMinBound(Integer.parseInt(minLevel.getValue()));
                levelItem.setCountParticipant(0);
                serviceManager.addLevel(levelItem);
                serviceManager.getLevels().forEach(serviceManager::updateCustomersLevel);
                levelItemGrid.setDataProvider(DataProvider.ofCollection(serviceManager.getLevels()));
                txtLevelName.clear();
                minLevel.clear();
                maxLevel.clear();
                comboBoxLevelPrize.setItems(serviceManager.levelNames());
            }
        });

        txtCountPrize = new TextField("Count");
        txtNamePrize = new TextField("Name");

        btnAddPrize = new Button("Add Prize", event -> {
            if (txtCountPrize.getValue().equalsIgnoreCase("") ||
                    txtNamePrize.getValue().equalsIgnoreCase("")) {
                txtCountPrize.setErrorMessage("Required");
                txtLevelName.setErrorMessage("Required");
            } else {
                Prize prize = new Prize();
                Optional<Level> level = serviceManager.getLevelByName(comboBoxLevelPrize.getValue());
                comboBoxLevelPrize.setValue(comboBoxLevelPrize.getValue());
                prize.setCountPrize(Integer.valueOf(txtCountPrize.getValue()));
                prize.setName(txtNamePrize.getValue());
                level.ifPresent(prize::setLevel);
                serviceManager.savePrize(prize);
                prizeGrid.setDataProvider(DataProvider.ofCollection(serviceManager.getAllPrize()));
                txtCountPrize.clear();
                txtNamePrize.clear();
                comboBoxLevelPrize.clear();
            }
        });
        divNewLevel.add(txtLevelName,createSpan(),minLevel,createSpan(),maxLevel,createSpan(),btnAddLevel,createSpan()
                ,txtNamePrize,createSpan(),comboBoxLevelPrize,createSpan(),txtCountPrize,createSpan(),btnAddPrize);
        levelItemGrid.setHeightByRows(true);
        prizeGrid.setHeightByRows(true);
        levelItemGrid.setWidth("500px");
        prizeGrid.setWidth("500px");
            gridLotteryResult.setWidth("1200px");
        txtStep = new TextField("Steps");
        txtLotteryName = new TextField("Lottery Name");
        btnProcess = new Button("Process", event -> {
            serviceManager.saveLottery(new Lottery(null,Integer.parseInt(txtStep.getValue()),txtLotteryName.getValue(),
                    Calendar.getInstance().getTimeInMillis()));
            serviceManager.executeLottery(Integer.valueOf(txtStep.getValue()));
            winners = serviceManager.getWinners();
            gridLotteryResult.setDataProvider(DataProvider.ofCollection(winners));
            serviceManager.SaveCustomer(winners);
        });
        divLotteryResult.add(txtLotteryName,createSpan(),createSpan(),createSpan(),
                txtStep,createSpan(),createSpan(),createSpan(),btnProcess);

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.add(levelItemGrid,createSpan(),createSpan(),createSpan(),createSpan(),
                createSpan(),createSpan(),createSpan(),createSpan(),createSpan(),createSpan(),
                createSpan(),prizeGrid);

        header = new Hr();
        header.setHeight("2px");
        gridLotteryResult.setHeightByRows(true);
        add(new H2("Lottery"),header,labelNumberTotalCustomer,
                divNewLevel,divNewPrize,horizontalLayout,
                divLotteryResult,createSpan(),createSpan(), gridLotteryResult);
    }

    private Span createSpan(){
        return new Span("          ");
    }
}
