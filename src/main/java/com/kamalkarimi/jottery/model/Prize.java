package com.kamalkarimi.jottery.model;

import lombok.*;
import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Setter @Getter @EqualsAndHashCode
@Entity @Table(name = "tbl_prizes")
public class Prize {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;

    private Integer countPrize;

    @ManyToOne
    private Level level;

    @Override
    public String toString() {
        return name;
    }
}
