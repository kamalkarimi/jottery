package com.kamalkarimi.jottery.model;

import lombok.*;

import javax.persistence.*;


@Entity
@Table(name = "tbl_levels")
@NoArgsConstructor @AllArgsConstructor
@Getter @Setter @EqualsAndHashCode

public class Level  {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(unique = true)
    private String  name;
    private Integer minBound;
    private Integer maxBound;
    private Integer countParticipant;


    @Override
    public String toString() {
        return  name ;
    }
}
