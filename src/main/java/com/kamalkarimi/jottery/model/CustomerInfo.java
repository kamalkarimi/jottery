package com.kamalkarimi.jottery.model;

import lombok.*;
import javax.persistence.*;


@Entity
@Table(name = "tbl_customers_info")
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class CustomerInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne
    private Level level;

    @ManyToOne
    private Prize prize;

    private String nationalCode;
    private String customerCode;
    private String name;
    private String lastName;
    private String birthday;
    private String gender;
    private Integer point;
    private String phoneNumber;

}




