package com.kamalkarimi.jottery.services;


import com.kamalkarimi.jottery.model.Prize;
import com.kamalkarimi.jottery.repository.PrizeRepository;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Data @NoArgsConstructor
@EqualsAndHashCode
@Service
public class PrizeService  {

    private PrizeRepository prizeRepository;
    private List<Prize> prizes = new ArrayList<>();
    @Autowired
    PrizeService(PrizeRepository prizeRepository){
        this.prizeRepository = prizeRepository;
    }

    public Prize savePrize(Prize prize){
        prizeRepository.save(prize);
        prizes.add(prize);
        return prize;
    }

    public List<Prize> findAll() {
        return (List<Prize>) prizeRepository.findAll();
    }
}
