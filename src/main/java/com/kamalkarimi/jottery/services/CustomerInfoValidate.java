package com.kamalkarimi.jottery.services;

import com.kamalkarimi.jottery.model.CustomerInfo;
import org.springframework.stereotype.Service;

import java.util.function.Predicate;

@Service
public class CustomerInfoValidate implements Predicate<CustomerInfo> {

    @Override
    public boolean test(CustomerInfo customerInfo) {
        return checkDate(customerInfo.getBirthday())&&checkedCustomerCode(customerInfo.getCustomerCode())&&
                checkedNationalCode(customerInfo.getNationalCode())&&checkedPhoneNumber(customerInfo.getPhoneNumber());
    }

    private Boolean checkedNationalCode(String nationalCode){
        return nationalCode.length()==10;
    }

    private Boolean checkedCustomerCode(String customerCode){
        return customerCode.length()==9;
    }

    private Boolean checkedPhoneNumber(String phoneNumber){
        return phoneNumber.length()==11 && phoneNumber.startsWith("09");
    }

    private Boolean checkDate(String birthday){
        String[] temp = birthday.split("/");
        Boolean result = false;
        if (Integer.parseInt(temp[0])>1900 && Integer.parseInt(temp[0])< 2019 &&
                Integer.parseInt(temp[1])>0 && Integer.parseInt(temp[1])<13 &&
                Integer.parseInt(temp[2])>0 && Integer.parseInt(temp[2])<32){
            result = true;
        }
        return result;
    }
}
