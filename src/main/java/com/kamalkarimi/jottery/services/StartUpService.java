package com.kamalkarimi.jottery.services;

import com.kamalkarimi.jottery.model.CustomerInfo;
import com.kamalkarimi.jottery.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

@Configuration
@PropertySource("classpath:dev.properties")
public class StartUpService {

    @Value("${isCustomerInfoIsImport}")
    private Boolean isCustomerInfoIsImport;

    @Value("${customerInfoFilePath}")
    private String customerInfoFilePath;

    @Value("${isCustomerWipeInfo}")
    private Boolean isCustomerWipeInfo;

    private CustomerRepository customerRepository;

    public StartUpService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @PostConstruct
    public void init() {

        if (isCustomerWipeInfo)
            wipeCustomerInfoFromDB();

        if (isCustomerInfoIsImport)
            readCustomerInfoFromFile(customerInfoFilePath);

    }

    private void wipeCustomerInfoFromDB() {
        customerRepository.deleteAll();
    }

    private void readCustomerInfoFromFile(String customerInfoFilePath) {
        try (BufferedReader reader = new BufferedReader(new FileReader(customerInfoFilePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] values = line.split(",");
                if (values.length != 8)
                    continue;
                CustomerInfo customerInfo = new CustomerInfo();
                customerInfo.setCustomerCode(values[0]);
                customerInfo.setNationalCode(values[1]);
                customerInfo.setName(values[2]);
                customerInfo.setLastName(values[3]);
                customerInfo.setPoint(Integer.valueOf(values[4]));
                customerInfo.setBirthday(values[5]);
                customerInfo.setGender(values[6]);
                customerInfo.setPhoneNumber(values[7]);
//                if (infoValidate.test(customerInfo)) {
                customerRepository.save(customerInfo);
//                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
