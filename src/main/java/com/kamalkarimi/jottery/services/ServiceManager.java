package com.kamalkarimi.jottery.services;

import com.kamalkarimi.jottery.model.CustomerInfo;
import com.kamalkarimi.jottery.model.Level;
import com.kamalkarimi.jottery.model.Lottery;
import com.kamalkarimi.jottery.model.Prize;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Getter
@Setter
@Slf4j
public class ServiceManager {

    private CustomerService customerService;
    private LevelService levelService;
    private PrizeService prizeService;
    private LotteryService lotteryService;
    private List<CustomerInfo> winners = new ArrayList<>();

    @Autowired
    public ServiceManager(CustomerService customerService, LevelService levelService,
                          PrizeService prizeService, LotteryService lotteryService) {
        this.customerService = customerService;
        this.levelService = levelService;
        this.prizeService = prizeService;
        this.lotteryService = lotteryService;
    }

    public Long totalCustomer() {
        return customerService.totalNumberCustomer();
    }

    public void updateCustomersLevel(Level level) {
        customerService.updateCustomerLevel(level);
    }

    public void addLevel(Level level) {
        levelService.addLevels(level);
    }

    public List<Level> getLevels() {
        return levelService.getLevels();
    }

    public List<String> levelNames() {
        List<String> names = new ArrayList<>();
        levelService.getLevels().forEach(item -> names.add(item.getName()));
        return names;
    }

    public Optional<Level> getLevelByName(String name) {
        return levelService.getLevelByName(name);
    }

    public Prize savePrize(Prize prize) {
        return prizeService.savePrize(prize);
    }

    public List<Prize> getAllPrize() {
        return prizeService.getPrizes();
    }

    public void numberCustomerInLevel(Level level) {
        levelService.numberCustomerInLevel(level);
    }

    public List<Level> fetchAllLevel() {
        return levelService.fetchAllLevel();
    }

    public void addCustomersInfo(List<CustomerInfo> infos) {
        customerService.addCustomersInfo(infos);
    }

    public List<CustomerInfo> findByPointBetween(Integer min, Integer max) {
        return customerService.findByPointBetween(min, max);
    }


    public Lottery saveLottery(Lottery lottery) {
        return lotteryService.saveLottery(lottery);
    }

    public List<Lottery> findAll() {
        return lotteryService.findAll();
    }

    public List<String> getPrizeName() {
        List<String> name = new ArrayList<>();
        prizeService.getPrizes().forEach(n -> name.add(n.getName()));
        return name;
    }

    public void executeLottery(Integer step) {
        List<CustomerInfo> customers = customerService.findAll();
        List<CustomerInfo> participants = new ArrayList<>();
        List<Prize> prizes = prizeService.findAll();
        Collections.shuffle(customers);
        for (CustomerInfo customer : customers) {
            for (int j = 0; j < customer.getPoint() / 100; j++) {
                participants.add(customer);
            }
        }
        for (Prize prize : prizes) {
            int count = 0;
            while (prize.getCountPrize() != count) {
                while (true) {
                    if (participants.get(step).getPrize() == null) {
                        if (participants.get(step).getLevel() != null &&
                                participants.get(step).getLevel().getId() != null &&
                                participants.get(step).getLevel().getId().equals(prize.getLevel().getId())) {
                            winners.add(participants.get(step));
                            participants.get(step).setPrize(prize);
                            step += step;
                            step = getNextStep(step, participants);
                            break;
                        } else {
                            step += step;
                            step = getNextStep(step, participants);
                        }
                    } else {
                        step++;
                        step = getNextStep(step, participants);
                    }
                }
                count++;
            }
        }
    }


    private Integer getNextStep(Integer step, List<CustomerInfo> participants) {
        if (!validStep(step))
            step = step % participants.size();
        return step;
    }

    private Boolean validStep(Integer step) {
        return step <= customerService.totalNumberCustomer();
    }

    public void SaveCustomer(List<CustomerInfo> winners) {
        customerService.save(winners);
    }
}
