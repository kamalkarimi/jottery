package com.kamalkarimi.jottery.services;

import com.kamalkarimi.jottery.model.CustomerInfo;
import com.kamalkarimi.jottery.model.Level;
import com.kamalkarimi.jottery.repository.CustomerRepository;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
@Getter @Setter @ToString
@EqualsAndHashCode
public class CustomerService {

    private CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public void addCustomersInfo(List<CustomerInfo> infos){
        infos.forEach(item->
            getCustomerRepository().save(item)
        );
    }

    public List<CustomerInfo> findByPointBetween(Integer min,Integer max){
        return customerRepository.findByPointBetween(min,max);
    }

    @Transactional
    public void updateCustomerLevel(Level level){
        customerRepository.updateLevel(level.getId(),
                level.getMinBound(), level.getMaxBound());
    }

    public Long totalNumberCustomer(){
        return customerRepository.count();
    }

    public List<CustomerInfo> findAll() {
        return (List<CustomerInfo>) customerRepository.findAll();
    }

    public void save(List<CustomerInfo> winners) {
        customerRepository.saveAll(winners);
    }
}
