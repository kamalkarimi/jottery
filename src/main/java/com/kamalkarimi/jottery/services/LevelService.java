package com.kamalkarimi.jottery.services;

import com.kamalkarimi.jottery.model.Level;
import com.kamalkarimi.jottery.repository.CustomerRepository;
import com.kamalkarimi.jottery.repository.LevelRepository;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@Getter @Setter
@EqualsAndHashCode
@ToString
public class LevelService  {

    private LevelRepository levelRepository;
    private CustomerRepository customerRepository;
    private List<Level> levels = new ArrayList<>();

    @Autowired
    public LevelService(LevelRepository levelRepository,CustomerRepository customerRepository) {
        this.levelRepository = levelRepository;
        this.customerRepository = customerRepository;
    }


    public void addLevels(Level level){
        levelRepository.save(level);
        levels.add(level);
    }

    public Integer getIdLevelBy(String levelName){
        return Math.toIntExact(levelRepository.getIDByName(levelName));
    }

    public void numberCustomerInLevel(Level level){
        Integer count = Math.toIntExact(customerRepository.countByPointBetween(level.getMinBound(), level.getMaxBound()));
        level.setCountParticipant(count);
        levelRepository.updateLevelByCountParticipant(count,getIdLevelBy(level.getName()));
    }

    public Optional<Level> getLevelByName(String name){
        return levelRepository.getLevelByName(name);
    }

    @Transactional
    public List<Level> fetchAllLevel(){
        levels = (List<Level>) levelRepository.findAll();
        return levels;
    }


    public List<String> levelName(){
        List<String> names = new ArrayList<>();
        getLevels().forEach(item->names.add(item.getName()));
        return names;
    }

    public List<Level> findAll() {
        return (List<Level>) levelRepository.findAll();
    }
}
