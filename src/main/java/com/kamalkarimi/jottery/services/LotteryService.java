package com.kamalkarimi.jottery.services;

import com.kamalkarimi.jottery.model.Lottery;
import com.kamalkarimi.jottery.repository.LotteryRepository;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Getter @Setter @EqualsAndHashCode
@ToString
public class LotteryService {

    private LotteryRepository lotteryRepository;

    @Autowired
    public LotteryService(LotteryRepository lotteryRepository) {
        this.lotteryRepository = lotteryRepository;
    }

    public Lottery saveLottery(Lottery lottery){
        return lotteryRepository.save(lottery);
    }

    public List<Lottery> findAll(){
        return (List<Lottery>) lotteryRepository.findAll();
    }
}
