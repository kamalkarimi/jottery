package com.kamalkarimi.jottery.repository;

import com.kamalkarimi.jottery.model.Prize;
import org.springframework.data.repository.CrudRepository;

public interface PrizeRepository extends CrudRepository<Prize,Long> {
}
