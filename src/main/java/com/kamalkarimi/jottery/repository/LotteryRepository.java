package com.kamalkarimi.jottery.repository;

import com.kamalkarimi.jottery.model.Lottery;
import org.springframework.data.repository.CrudRepository;

public interface LotteryRepository extends CrudRepository<Lottery,Long> {
}
