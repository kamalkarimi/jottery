package com.kamalkarimi.jottery.repository;

import com.kamalkarimi.jottery.model.Level;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
@Repository
public interface LevelRepository extends CrudRepository<Level,Long> {

    @Query(value = "SELECT level.id FROM Level level WHERE level.name = :name")
    Long getIDByName(@Param("name") String name);

    @Modifying
    @Query(value = "UPDATE Level AS level SET level.countParticipant = :countParticipant WHERE level.id = :levelId")
    void updateLevelByCountParticipant(@Param("countParticipant") Integer countParticipant,@Param("levelId") Integer id);

    Optional<Level> getLevelByName(String name);
}
