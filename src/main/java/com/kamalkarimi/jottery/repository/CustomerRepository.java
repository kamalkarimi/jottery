package com.kamalkarimi.jottery.repository;

import com.kamalkarimi.jottery.model.CustomerInfo;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CustomerRepository extends CrudRepository<CustomerInfo,Long> {

    Integer countByPointBetween(Integer min, Integer max);
    List<CustomerInfo> findByPointBetween(Integer min,Integer max);

    @Modifying
    @Query(value = "UPDATE CustomerInfo AS info SET info.level.id = :levelId WHERE info.point BETWEEN :minBound AND :maxBound")
    void updateLevel(@Param("levelId") Integer levelId, @Param("minBound") Integer minBound ,@Param("maxBound")  Integer maxBound);


}
